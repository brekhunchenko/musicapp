//
//  TexturesManager.h
//  MusicApp
//
//  Created by Yaroslav on 7/6/16.
//  Copyright © 2016 hyh. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <SpriteKit/SpriteKit.h>

#import "DrummerDefines.h"

@interface TexturesManager : NSObject

+ (instancetype)sharedInstance;

- (NSArray<SKTexture *> *)faceTexturesForExpression:(DrummerExpression)expression
                                  headbangIntensity:(HeadbangIntensity)intensity;

@property (nonatomic, strong, readonly) NSArray<SKTexture *> *listenWaitPlayTextures;
@property (nonatomic, strong, readonly) NSArray<SKTexture *> *waitLightTextures;

@property (nonatomic, strong, readonly) NSArray<SKTexture *> *leftConfettiTextures;
@property (nonatomic, strong, readonly) NSArray<SKTexture *> *rightConfettiTextures;

@property (nonatomic, strong, readonly) NSArray<SKTexture *> *leftSmokeTextures;
@property (nonatomic, strong, readonly) NSArray<SKTexture *> *rightSmokeTextures;

@property (nonatomic, strong, readonly) NSArray<SKTexture *> *leftArmTextures;
@property (nonatomic, strong, readonly) NSArray<SKTexture *> *rightArmTextures;
@property (nonatomic, strong, readonly) NSArray<SKTexture *> *leftDishTextures;
@property (nonatomic, strong, readonly) NSArray<SKTexture *> *rightDishTextures;

@property (nonatomic, strong, readonly) NSArray<SKTexture *> *drumsetHitTextures;

@end
