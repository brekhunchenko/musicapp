//
//  AvatarView.h
//  MusicApp
//
//  Created by hyh on 6/4/16.
//  Copyright © 2016 hyh. All rights reserved.
//

#import <SpriteKit/SpriteKit.h>
#import "DrummerDefines.h"
#import "ListenWaitPlaySprite.h"
#import "ConductorController.h"

typedef NS_ENUM(NSInteger, SceneBackgroundStyle) {
  SceneBackgroundStyleDefault,
  SceneBackgroundStyleBoss
};

@interface CharacterMusicScene : SKScene

@property (nonatomic, assign) CGFloat beatDuration;

- (void)showDrummerExpressionAnimation:(DrummerExpression)expression
                 withHeadbangIntensity:(HeadbangIntensity)intensity;

- (void)showRightHandStrikeAnimation;
- (void)showLeftHandStrikeAnimation;

- (void)changeBackgroundStyleTo:(SceneBackgroundStyle)sceneStyle;

- (void)turnOnListenLightAnimation;
- (void)turnOnWaitLightAnimation;
- (void)turnOnPlayLightAnimaiton;
- (void)turnOffListenWaitPlayLightsAnimation;

- (void)showConfettiAnimation;

- (void)showSmokeAnimation;

@end
