//
//  ListenWaitPlaySprite.h
//  MusicApp
//
//  Created by Yaroslav on 7/7/16.
//  Copyright © 2016 hyh. All rights reserved.
//

#import <SpriteKit/SpriteKit.h>

@interface ListenWaitPlaySprite : SKSpriteNode

- (void)turnOffLights;

- (void)turnOnListenLight;
- (void)turnOnWaitLight;
- (void)turnOnPlayLight;

@end
