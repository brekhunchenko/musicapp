//
//  GameScene.m
//  MusicApp
//
//  Created by hyh on 11/13/15.
//  Copyright (c) 2015 hyh. All rights reserved.
//

#import "GameScene.h"
#import "ConductorController.h"
#import "CharacterMusicScene.h"

@interface GameScene()

@property (nonatomic, assign) DrummerExpression currentExpression;
@property (nonatomic, assign) HeadbangIntensity currentIntensity;

@property (nonatomic, strong) SKLabelNode* devilLabel;
@property (nonatomic, strong) SKLabelNode* happyLabel;
@property (nonatomic, strong) SKLabelNode* joyLabel;
@property (nonatomic, strong) SKLabelNode* neutralLabel;
@property (nonatomic, strong) SKLabelNode* sad0Label;
@property (nonatomic, strong) SKLabelNode* sad1lLabel;
@property (nonatomic, strong) SKLabelNode* seriousLabel;

@property (nonatomic, strong) SKLabelNode* intensityNoneLabel;
@property (nonatomic, strong) SKLabelNode* intensityLightLabel;
@property (nonatomic, strong) SKLabelNode* intensityMediumLabel;
@property (nonatomic, strong) SKLabelNode* intensityStrongLabel;

@property (nonatomic, strong) SKLabelNode* showListenWaitStopSprite;
@property (nonatomic, strong) SKLabelNode* showWaitSprite;
@property (nonatomic, strong) SKLabelNode* showPlaySprite;
@property (nonatomic, strong) SKLabelNode* hideListenWaitStopSprite;

@property (nonatomic, strong) SKLabelNode* showBossSceneStyle;
@property (nonatomic, strong) SKLabelNode* showDefaultSceneStyle;

@property (nonatomic, strong) SKLabelNode* showConfettiLabel;
@property (nonatomic, strong) SKLabelNode* showSmokeLabel;

@property (nonatomic, strong) SKLabelNode* debugLabel;

@property (nonatomic, strong) ConductorController* conductor;

@end

@interface GameScene() < ConductorControllerDelegate >
@end

@implementation GameScene {
    double _previousTime;
}

#pragma mark - SKScene Life Cycle

- (void)didMoveToView:(SKView *)view {
  [super didMoveToView:view];
  
  _devilLabel = [self addLabel:@"Devil" position:CGPointMake(16.0f, self.scene.size.height - 16.0f)];
  _happyLabel = [self addLabel:@"Happy" position:CGPointMake(16.0f, _devilLabel.position.y - 44.0f)];
  _joyLabel = [self addLabel:@"Joy" position:CGPointMake(16.0f, _happyLabel.position.y - 44.0f)];
  _neutralLabel = [self addLabel:@"Neutral" position:CGPointMake(16.0f, _joyLabel.position.y - 44.0f)];
  _sad0Label = [self addLabel:@"Sad0" position:CGPointMake(16.0f, _neutralLabel.position.y - 44.0f)];
  _sad1lLabel = [self addLabel:@"Sad1" position:CGPointMake(16.0f, _sad0Label.position.y - 44.0f)];
  _seriousLabel = [self addLabel:@"Serious" position:CGPointMake(16.0f, _sad1lLabel.position.y - 44.0f)];

  _intensityLightLabel = [self addLabel:@"Light" position:CGPointMake(130.0f, self.scene.size.height - 16.0f)];
  _intensityMediumLabel = [self addLabel:@"Medium" position:CGPointMake(130.0f, _intensityLightLabel.position.y - 44.0f)];
  _intensityStrongLabel = [self addLabel:@"Strong" position:CGPointMake(130.0f, _intensityMediumLabel.position.y - 44.0f)];
  _intensityNoneLabel = [self addLabel:@"None" position:CGPointMake(130.0f, _intensityStrongLabel.position.y - 44.0f)];
  
  _showConfettiLabel = [self addLabel:@"Confetti"
                             position:CGPointMake(self.scene.size.width - 300.0f, self.scene.size.height - 16.0f)];
  _showSmokeLabel = [self addLabel:@"Smoke"
                             position:CGPointMake(self.scene.size.width - 300.0f, _showConfettiLabel.position.y - 44.0f)];
  _showBossSceneStyle = [self addLabel:@"Show BOSS Scene Style"
                              position:CGPointMake(self.scene.size.width - 300.0f, _showSmokeLabel.position.y - 44.0f)];
  _showDefaultSceneStyle = [self addLabel:@"Show Default Scene Style"
                                 position:CGPointMake(self.scene.size.width - 300.0f, _showBossSceneStyle.position.y - 44.0f)];


  _hideListenWaitStopSprite = [self addLabel:@"Hide Listen Wait Play"
                                    position:CGPointMake(16.0f, 44.0f)];
  _showPlaySprite = [self addLabel:@"Show Play"
                          position:CGPointMake(16.0f, _hideListenWaitStopSprite.position.y + 44.0f)];
  _showWaitSprite = [self addLabel:@"Show Wait"
                          position:CGPointMake(16.0f, _showPlaySprite.position.y + 44.0f)];
  _showListenWaitStopSprite = [self addLabel:@"Show Listen"
                                    position:CGPointMake(16.0f, _showWaitSprite.position.y  + 44.0f)];

//  _debugLabel = [self addLabel:@""
//                      position:CGPointMake(16.0f, 32.0f)];

  _conductor = [ConductorController new];
  _conductor.delegate = self;
  [_conductor startConduct];
  
  _currentExpression = DrummerExpressionNeutral;
  _currentIntensity = HeadbangIntensityMedium;
  [_debugLabel setText:[NSString stringWithFormat:@"Expression: %@   Intensity: %@", NSStingFromExpression(_currentExpression),
                        NSStringFromIntensity(_currentIntensity)]];
}

#pragma mark - Helpers

- (SKLabelNode*)addLabel:(NSString *)text position:(CGPoint)position {
  SKLabelNode *label = [SKLabelNode labelNodeWithFontNamed:@"Chalkduster"];
  label.text = text;
  label.fontSize = 20;
  label.horizontalAlignmentMode = SKLabelHorizontalAlignmentModeLeft;
  label.verticalAlignmentMode = SKLabelVerticalAlignmentModeTop;
  label.zPosition = 10;
  label.position = position;
  label.fontColor = [SKColor blackColor];
  [self addChild:label];
  return label;
}

#pragma mark - Touches

- (void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event {
  UITouch *touch = [touches anyObject];
  CGPoint location = [touch locationInNode:self];
  SKNode *node = [self nodeAtPoint:location];

  if ([node isEqual:_devilLabel]) {
    _currentExpression = DrummerExpressionDevil;
  } else if ([node isEqual:_happyLabel]) {
    _currentExpression = DrummerExpressionHappy;
  } else if ([node isEqual:_joyLabel]) {
    _currentExpression = DrummerExpressionJoy;
  } else if ([node isEqual:_neutralLabel]) {
    _currentExpression = DrummerExpressionNeutral;
  } else if ([node isEqual:_sad0Label]) {
    _currentExpression = DrummerExpressionSad0;
  } else if ([node isEqual:_sad1lLabel]) {
    _currentExpression = DrummerExpressionSad1;
  } else if ([node isEqual:_seriousLabel]) {
    _currentExpression = DrummerExpressionSerious;
  }

  if ([node isEqual:_intensityLightLabel]) {
    _currentIntensity = HeadbangIntensityLight;
  } else if ([node isEqual:_intensityMediumLabel]) {
    _currentIntensity = HeadbangIntensityMedium;
  } else if ([node isEqual:_intensityStrongLabel]) {
    _currentIntensity = HeadbangIntensityStrong;
  } else if ([node isEqual:_intensityNoneLabel]) {
    _currentIntensity = HeadbangIntensityNone;
  }

  if ([node isEqual:_showListenWaitStopSprite]) {
    [self turnOnListenLightAnimation];
  } else if ([node isEqual:_hideListenWaitStopSprite]) {
    [self turnOffListenWaitPlayLightsAnimation];
  } else if ([node isEqual:_showWaitSprite]) {
    [self turnOnWaitLightAnimation];
  } else if ([node isEqual:_showPlaySprite]) {
    [self turnOnPlayLightAnimaiton];
  }
  
  if ([node isEqual:_showBossSceneStyle]) {
    [self changeBackgroundStyleTo:SceneBackgroundStyleBoss];
  } else if ([node isEqual:_showDefaultSceneStyle]) {
    [self changeBackgroundStyleTo:SceneBackgroundStyleDefault];
  }
  
  if ([node isEqual:_showConfettiLabel]) {
    [self showConfettiAnimation];
  } else if ([node isEqual:_showSmokeLabel]) {
    [self showSmokeAnimation];
  }
  
  [_debugLabel setText:[NSString stringWithFormat:@"Expression: %@   Intensity: %@", NSStingFromExpression(_currentExpression),
                        NSStringFromIntensity(_currentIntensity)]];
}

- (void)update:(CFTimeInterval)currentTime {
  if (_previousTime) {
    [_conductor update:currentTime - _previousTime];
  }

  _previousTime = currentTime;
}

#pragma mark - ConductorControllerDelegate

- (void)update:(float)dt conductor:(ConductorController *)conductor {
  self.beatDuration = conductor.beatDuration;
  
  float timePerFrame = MIN(self.beatDuration/4.0, 0.1);
  
  if (conductor.getCurrentBeatTime + timePerFrame >= conductor.beatDuration) {
    [self showRightHandStrikeAnimation];
  }
  
  if (conductor.getCurrentNoteTime + 0.1 >= conductor.noteDuration) {
    [self showDrummerExpressionAnimation:self.currentExpression
                   withHeadbangIntensity:self.currentIntensity];
    [self showLeftHandStrikeAnimation];
  }
}

@end
