//
//  AvatarView.m
//  MusicApp
//
//  Created by hyh on 6/4/16.
//  Copyright © 2016 hyh. All rights reserved.
//

#import "CharacterMusicScene.h"
#import "ConductorController.h"
#import "TexturesManager.h"

@interface CharacterMusicScene()

@property (nonatomic, strong) ListenWaitPlaySprite *listenWaitPlay;

@property (nonatomic, strong) NSArray<SKSpriteNode *> *bossStyleLights;
@property (nonatomic, strong) SKSpriteNode* background;
@property (nonatomic, strong) SKSpriteNode* drumset;
@property (nonatomic, strong) SKSpriteNode* projectorLight;
@property (nonatomic, strong) SKSpriteNode* bossLines;

@property (nonatomic, strong) SKSpriteNode* face;
@property (nonatomic, strong) SKSpriteNode* leftArm;
@property (nonatomic, strong) SKSpriteNode* rightArm;
@property (nonatomic, strong) SKSpriteNode* leftDish;
@property (nonatomic, strong) SKSpriteNode* rightDish;

@property (nonatomic, strong) SKSpriteNode* confettiLeft;
@property (nonatomic, strong) SKSpriteNode* confettiRight;

@property (nonatomic, strong) SKSpriteNode* smokeLeft;
@property (nonatomic, strong) SKSpriteNode* smokeRight;

@end

@implementation CharacterMusicScene

#pragma mark - Initializers

- (void)didMoveToView:(SKView *)view {
  [super didMoveToView:view];
  
  [self _setupAnimationSprites];
}

#pragma mark - Setup

- (void)_setupAnimationSprites {
  _background = (SKSpriteNode *)[self.scene childNodeWithName:@"background"];
  
  [self _setupBodySprites];
  [self _setupLeftArmSprite];
  [self _setupRightArmSprites];
  [self _setupConfettiSprites];
  [self _setupDrumSetSprites];
  [self _setupBossEpisodeSpritesSprite];
  [self _setupListenWaitPlaySprites];
  [self _setupSmokeSprite];
}

- (void)_setupListenWaitPlaySprites {
  _listenWaitPlay = (ListenWaitPlaySprite *)[self.scene childNodeWithName:@"ListenWaitPlay"];
  _listenWaitPlay.anchorPoint = CGPointMake(0.5f, 0.0f);
  _listenWaitPlay.position = CGPointMake(self.scene.size.width/2.0f, self.scene.size.height);
}

- (void)_setupConfettiSprites {
  _confettiLeft = (SKSpriteNode *)[self.scene childNodeWithName:@"confettiLeft"];
  _confettiLeft.hidden = YES;
  _confettiRight = (SKSpriteNode *)[self.scene childNodeWithName:@"confettiRight"];
  _confettiRight.hidden = YES;
}

- (void)_setupBossEpisodeSpritesSprite {
  NSMutableArray* lightsMutable = [NSMutableArray new];
  for (int i = 0; i < 6; i++) {
    SKSpriteNode* lightSprite = (SKSpriteNode *)[self.scene childNodeWithName:[NSString stringWithFormat:@"bossLight_%d", i]];
    lightSprite.alpha = 0.0f;
    [lightsMutable addObject:lightSprite];
  }
  _bossStyleLights = [NSArray arrayWithArray:lightsMutable];
  
  _bossLines = [[SKSpriteNode alloc] initWithTexture:[SKTexture textureWithImageNamed:@"boss_lines"]
                                               color:[UIColor blackColor]
                                                size:CGSizeMake(self.scene.size.width, self.scene.size.width*0.4392f)];
  _bossLines.anchorPoint = CGPointMake(0.5f, 0.0f);
  _bossLines.position = CGPointMake(self.scene.size.width/2.0f,
                                    self.scene.size.height - _bossLines.size.height - 50.0f);
  _bossLines.alpha = 0.0f;
  [self.scene addChild:_bossLines];
  
  _projectorLight = [[SKSpriteNode alloc] initWithTexture:[SKTexture textureWithImageNamed:@"boss_projector_light"]
                                                    color:[UIColor blackColor]
                                                     size:CGSizeMake(931.0f, 683.0f)];
  _projectorLight.anchorPoint = CGPointMake(0.5f, 0.0f);
  _projectorLight.position = CGPointMake(self.scene.size.width/2.0f,
                                         self.scene.size.height - _projectorLight.size.height);
  _projectorLight.alpha = 0.0f;
  [self.scene addChild:_projectorLight];
}

- (void)_setupBodySprites {
  _face = (SKSpriteNode *)[self.scene childNodeWithName:@"face"];
}

- (void)_setupLeftArmSprite {
  _leftArm = (SKSpriteNode *)[self.scene childNodeWithName:@"left_arm"];
  _leftArm.texture = [TexturesManager sharedInstance].leftArmTextures.firstObject;
  
  _leftDish = (SKSpriteNode *)[self.scene childNodeWithName:@"left_dish"];
}

- (void)_setupRightArmSprites {
  _rightArm = (SKSpriteNode *)[self.scene childNodeWithName:@"right_arm"];
  _rightArm.texture = [TexturesManager sharedInstance].rightArmTextures.firstObject;
  
  _rightDish = (SKSpriteNode *)[self.scene childNodeWithName:@"right_dish"];
}

- (void)_setupDrumSetSprites {
  _drumset = (SKSpriteNode *)[self.scene childNodeWithName:@"drumset"];
}

- (void)_setupSmokeSprite {
  _smokeLeft = (SKSpriteNode *)[self.scene childNodeWithName:@"smokeLeft"];
  _smokeLeft.hidden = YES;
  _smokeRight = (SKSpriteNode *)[self.scene childNodeWithName:@"smokeRight"];
  _smokeRight.hidden = YES;
}

#pragma mark - Class Methods

- (void)showConfettiAnimation {
  _confettiLeft.hidden = NO;
  _confettiRight.hidden = NO;
  [_confettiLeft runAction:[SKAction animateWithTextures:[[TexturesManager sharedInstance] leftConfettiTextures]
                                            timePerFrame:0.5f] completion:^{
    _confettiLeft.hidden = YES;
  }];
  [_confettiRight runAction:[SKAction animateWithTextures:[[TexturesManager sharedInstance] rightConfettiTextures]
                                            timePerFrame:0.5f] completion:^{
    _confettiRight.hidden = YES;
  }];
}

- (void)showSmokeAnimation {
  _smokeLeft.hidden = NO;
  _smokeRight.hidden = NO;

  [_smokeLeft runAction:[SKAction animateWithTextures:[TexturesManager sharedInstance].leftSmokeTextures timePerFrame:0.1f] completion:^{
    _smokeLeft.hidden = YES;
  }];
  [_smokeRight runAction:[SKAction animateWithTextures:[TexturesManager sharedInstance].rightSmokeTextures timePerFrame:0.1f] completion:^{
    _smokeRight.hidden = YES;
  }];
}

- (void)turnOnListenLightAnimation {
  [_listenWaitPlay runAction:[SKAction repeatActionForever:[SKAction animateWithTextures:[TexturesManager sharedInstance].listenWaitPlayTextures
                                                                            timePerFrame:0.15f]]];

  SKAction* moveAction = [SKAction moveTo:CGPointMake(self.scene.size.width/2.0f,
                                                      self.scene.size.height - _listenWaitPlay.size.height)
                                 duration:0.5f];
  [_listenWaitPlay runAction:moveAction completion:^{
    [_listenWaitPlay turnOnListenLight];
  }];
}

- (void)turnOnWaitLightAnimation {
  [_listenWaitPlay turnOnWaitLight];
}

- (void)turnOnPlayLightAnimaiton {
  [_listenWaitPlay turnOnPlayLight];
}

- (void)turnOffListenWaitPlayLightsAnimation {
  SKAction* moveAction = [SKAction moveTo:CGPointMake(self.scene.size.width/2.0f,
                                                      self.scene.size.height)
                                 duration:0.5f];
  [_listenWaitPlay runAction:moveAction completion:^{
    [_listenWaitPlay turnOffLights];
    [_listenWaitPlay removeAllActions];
  }];
}

- (void)changeBackgroundStyleTo:(SceneBackgroundStyle)sceneStyle {
  if (sceneStyle == SceneBackgroundStyleBoss) {
    _background.texture = [SKTexture textureWithImageNamed:@"boss stage1"];
    [_bossLines runAction:[SKAction sequence:@[[SKAction waitForDuration:0.5f],
                                               [SKAction fadeInWithDuration:0.15f]]]];
    [_projectorLight runAction:[SKAction sequence:@[[SKAction waitForDuration:2.2],
                                                    [SKAction fadeInWithDuration:0.15f]]]];
    for (int i = 0; i < _bossStyleLights.count; i++) {
      SKSpriteNode* bossLightSprite = _bossStyleLights[i];
      CGFloat delay = 1.0f;
      if (i >= 2 && i < 4) {
        delay = 1.5f;
      } else if (i < 2) {
        delay = 2.0f;
      }
      [bossLightSprite runAction:[SKAction sequence:@[[SKAction waitForDuration:delay],
                                                [SKAction fadeInWithDuration:0.15f]]]];
    }
  } else if (sceneStyle == SceneBackgroundStyleDefault) {
    _background.texture = [SKTexture textureWithImageNamed:@"background"];
    for (SKSpriteNode* bossLightSprite in _bossStyleLights) {
      [bossLightSprite removeAllActions];
      bossLightSprite.alpha = 0.0f;
    }
    [_projectorLight removeAllActions];
    _projectorLight.alpha = 0.0f;
    [_bossLines removeAllActions];
    _bossLines.alpha = 0.0f;
  }
}

- (void)showDrummerExpressionAnimation:(DrummerExpression)expression
                 withHeadbangIntensity:(HeadbangIntensity)intensity {
  if (![_face hasActions]) {
    NSArray* faceTextures = [[TexturesManager sharedInstance] faceTexturesForExpression:expression
                                                                      headbangIntensity:intensity];
    [self animateNode:_face
         withTextures:faceTextures
         beatDuration:self.beatDuration];
  }
}

- (void)showLeftHandStrikeAnimation {
  if (![_leftArm hasActions]) {
    [self animateNode:_leftArm
         withTextures:[TexturesManager sharedInstance].leftArmTextures
         beatDuration:self.beatDuration];
  }
  
  if (![_leftDish hasActions])
    [self animateNode:_leftDish
         withTextures:[TexturesManager sharedInstance].leftDishTextures
         beatDuration:self.beatDuration];
  
  if (![_drumset hasActions]) {
    [self animateNode:_drumset
         withTextures:[TexturesManager sharedInstance].drumsetHitTextures
         beatDuration:self.beatDuration];
  }
}

- (void)showRightHandStrikeAnimation {
  if (![_rightArm hasActions]) {
    [self animateNode:_rightArm
         withTextures:[TexturesManager sharedInstance].rightArmTextures
         beatDuration:self.beatDuration];
  }
  
  if (![_rightDish hasActions]) {
    [self animateNode:_rightDish
         withTextures:[TexturesManager sharedInstance].rightDishTextures
         beatDuration:self.beatDuration];
  }
}

#pragma mark - Helpers

- (void)animateNode:(SKNode *)node
       withTextures:(NSArray *)textures
       beatDuration:(float)beatDuration {
  float timePerFrame = MIN(beatDuration/2.0f, 0.1f);
  
  [node removeAllActions];
  SKAction *action = [SKAction animateWithTextures:textures timePerFrame:timePerFrame];
  [node runAction:action];
}

@end
