//
//  ConductorController.m
//  MusicApp
//
//  Created by hyh on 11/15/15.
//  Copyright (c) 2015 hyh. All rights reserved.
//

#import "ConductorController.h"


@implementation ConductorController
{
    int _beatNumber;
    
    float _beatDuration;
    
    float _noteDuration;
    
    int _currentBeat;
    
    float _currentBeatTime;
    
    float _currentNoteTime;
    
    BOOL _isConducting;    
}

-(id)init
{
    self = [super init];
    
    if (self)
    {
        [self initConductor];
    }
    
    return self;
}

-(void)initConductor
{
    _beatNumber = 4;
    _beatDuration = 0.5;
    _noteDuration = 1;

    [self reset];
}

-(void)reset
{
    _currentBeat = 0;
    _beatDuration = 0.5;
    _noteDuration = 1;
    _isConducting = false;
}

-(BOOL)isConducting
{
    return _isConducting;
}

-(void)stopConduct
{
    _isConducting = false;
    
    [self reset];
}

-(void)startConduct
{
    _isConducting = true;
}



-(float)beatDuration
{
    return _beatDuration;
}

-(void)setBeatDuration:(float)duration
{
    _beatDuration = duration;
}

-(int)getCurrentBeat
{
    return _currentBeat;
}

-(float)getCurrentBeatTime
{
    return _currentBeatTime;
}


-(void)update:(float)dt
{
    if (!_isConducting) return;
    
    _currentBeatTime += dt;
    
    while (_currentBeatTime > _beatDuration)
    {
        _currentBeat = (_currentBeat + 1) % _beatNumber;
        _currentBeatTime -= _beatDuration;
    }
    
    _currentNoteTime += dt;
    
    while (_currentNoteTime > _noteDuration)
    {
        _currentNoteTime -= _noteDuration;
    }
    
    [self.delegate update:dt conductor:self];
}

-(float)noteDuration
{
    return _noteDuration;
}

-(void)setNoteDuration:(float)noteDuration
{
    _noteDuration = noteDuration;
}

-(float)getCurrentNoteTime
{
    return _currentNoteTime;
}

@end
