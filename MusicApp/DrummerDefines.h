//
//  DrummerDefines.h
//  MusicApp
//
//  Created by Yaroslav on 7/6/16.
//  Copyright © 2016 hyh. All rights reserved.
//

#ifndef DrummerDefines_h
#define DrummerDefines_h

typedef NS_ENUM(NSInteger, DrummerExpression) {
  DrummerExpressionDevil,
  DrummerExpressionHappy,
  DrummerExpressionJoy,
  DrummerExpressionNeutral,
  DrummerExpressionSad0,
  DrummerExpressionSad1,
  DrummerExpressionSerious
};

typedef NS_ENUM(NSInteger, HeadbangIntensity) {
  HeadbangIntensityNone,
  HeadbangIntensityLight,
  HeadbangIntensityMedium,
  HeadbangIntensityStrong,
};

static __unused NSString* NSStingFromExpression(DrummerExpression expression) {
  NSString* expressionString = nil;
  switch (expression) {
    case DrummerExpressionDevil: expressionString = @"devilish"; break;
    case DrummerExpressionJoy: expressionString = @"joy"; break;
    case DrummerExpressionSad0: expressionString = @"sad"; break;
    case DrummerExpressionSad1: expressionString = @"sad2"; break;
    case DrummerExpressionSerious: expressionString = @"serious"; break;
    case DrummerExpressionHappy: expressionString = @"happy"; break;
    case DrummerExpressionNeutral: expressionString = @"neutral"; break;
  }
  assert(expressionString.length > 0);
  return expressionString;
}

static __unused DrummerExpression expressionFromString(NSString* expressionString) {
  DrummerExpression expression = DrummerExpressionHappy;
  if ([expressionString isEqualToString:NSStingFromExpression(DrummerExpressionDevil)]) {
    expression = DrummerExpressionDevil;
  } else if ([expressionString isEqualToString:NSStingFromExpression(DrummerExpressionJoy)]) {
    expression = DrummerExpressionJoy;
  } else if ([expressionString isEqualToString:NSStingFromExpression(DrummerExpressionSad0)]) {
    expression = DrummerExpressionSad0;
  } else if ([expressionString isEqualToString:NSStingFromExpression(DrummerExpressionSad1)]) {
    expression = DrummerExpressionSad1;
  } else if ([expressionString isEqualToString:NSStingFromExpression(DrummerExpressionSerious)]) {
    expression = DrummerExpressionSerious;
  } else if ([expressionString isEqualToString:NSStingFromExpression(DrummerExpressionHappy)]) {
    expression = DrummerExpressionHappy;
  } else if ([expressionString isEqualToString:NSStingFromExpression(DrummerExpressionNeutral)]) {
    expression = DrummerExpressionNeutral;
  } else {
    assert(false);
  }
  return expression;
}

static __unused NSString* NSStringFromIntensity(HeadbangIntensity intensity) {
  NSString* intensityString = nil;
  switch (intensity) {
    case HeadbangIntensityStrong: intensityString = @"headbang"; break;
    case HeadbangIntensityMedium: intensityString = @"middle"; break;
    case HeadbangIntensityLight: intensityString = @"light"; break;
    case HeadbangIntensityNone: intensityString = @"none"; break;
  }
  assert(intensityString.length > 0);
  return intensityString;
}

static __unused HeadbangIntensity headbangIntensityFromString(NSString* headbanhIntensityString) {
  HeadbangIntensity intensity = HeadbangIntensityStrong;
  if ([headbanhIntensityString isEqualToString:NSStringFromIntensity(HeadbangIntensityStrong)]) {
    intensity = HeadbangIntensityStrong;
  } else if ([headbanhIntensityString isEqualToString:NSStringFromIntensity(HeadbangIntensityMedium)]) {
    intensity = HeadbangIntensityMedium;
  } else if ([headbanhIntensityString isEqualToString:NSStringFromIntensity(HeadbangIntensityLight)]) {
    intensity = HeadbangIntensityLight;
  }
  return intensity;
}

#endif /* DrummerDefines_h */
