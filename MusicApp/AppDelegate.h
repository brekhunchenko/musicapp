//
//  AppDelegate.h
//  MusicApp
//
//  Created by hyh on 11/13/15.
//  Copyright (c) 2015 hyh. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;


@end

