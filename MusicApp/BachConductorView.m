//
//  BachConductorView.m
//  MusicApp
//
//  Created by hyh on 6/4/16.
//  Copyright © 2016 hyh. All rights reserved.
//

#import "BachConductorView.h"
#import "BeatData.h"

@implementation BachConductorView
{
    float _beatDuration;
    
    int _numBeats;
    
    int _currentBeat;
    
    SKSpriteNode *_leftHand;
    SKSpriteNode *_rightHand;
    
    CGPoint _leftHandPosition;
    CGPoint _rightHandPosition;
    
    float *_beatX;
    float *_beatY;
    
    BOOL _isDown;
}

-(SKSpriteNode*)leftHand
{
    return _leftHand;
}


-(SKSpriteNode*)rightHand
{
    return _rightHand;
}


-(void)setLeftHand:(SKSpriteNode *)leftHand
{
    _leftHand = leftHand;
    _leftHandPosition = leftHand.position;
}

-(void)setRightHand:(SKSpriteNode *)rightHand
{
    _rightHand = rightHand;
    _rightHandPosition = rightHand.position;
}

-(void)startConductWith:(SKSpriteNode *)sprite
{
    CGPoint oldPoint;
    
    if (sprite == _leftHand)
    {
        oldPoint = _leftHandPosition;
    }
    else
    {
        oldPoint = _rightHandPosition;
    }
    
    
    float duration = _beatDuration * 0.5;
    float y1 = _beatY[_currentBeat];
    float y2 = _beatY[(_currentBeat + 1) % _numBeats];
    
    float x1 = _beatX[_currentBeat];
    float x2 = _beatX[(_currentBeat + 1) % _numBeats];
    
    if (sprite == _leftHand)
    {
        x1 = -x1;
        x2 = -x2;
    }
    
    SKAction *actionYDown = [SKAction customActionWithDuration:duration actionBlock
                                                              :^(SKNode *sprite, CGFloat elapsedTime){
                                                                  
                                                                  float a = y1 / (duration * duration);
                                                                  float dy = -a * elapsedTime * elapsedTime + y1;
                                                                  sprite.position = CGPointMake(sprite.position.x, oldPoint.y + dy);
                                                                  
                                                                  _isDown = true;
                                                              }];
    
    SKAction *actionYUp = [SKAction customActionWithDuration:duration actionBlock
                                                            :^(SKNode *sprite, CGFloat elapsedTime){
                                                                
                                                                float a = y2 / (duration * duration);
                                                                float dy = -a * (elapsedTime - duration) * (elapsedTime - duration) + y2;
                                                                sprite.position = CGPointMake(sprite.position.x, oldPoint.y + dy);
                                                                
                                                                _isDown = false;
                                                            }];
    
    
    SKAction *actionY = [SKAction sequence:@[actionYDown, actionYUp]];
    
    SKAction *actionX = [SKAction moveToX:oldPoint.x + x2 duration:_beatDuration];
    actionX.timingMode = SKActionTimingEaseInEaseOut;
    
    SKAction *action = [SKAction group:@[actionX, actionY]];
    
    [sprite runAction:action completion:^(){
        
        if (sprite == _leftHand)
        {
            _currentBeat = (_currentBeat + 1) % _numBeats;
            //[self start];
        }
        
        
    }];
}

- (void)reset
{
    _leftHand.position = _leftHandPosition;
    _rightHand.position = _rightHandPosition;
}

-(void)switchBeat
{
    //[self stopConduct];
    
    switch (_numBeats)
    {
        case 4:
            _numBeats = 6;
            _beatX = Beat6X;
            _beatY = Beat6Y;
            break;
        case 6:
            _numBeats = 2;
            _beatX = Beat2X;
            _beatY = Beat2Y;
            break;
        case 2:
            _numBeats = 3;
            _beatX = Beat3X;
            _beatY = Beat3Y;
            break;
        case 3:
            _numBeats = 4;
            _beatX = Beat4X;
            _beatY = Beat4Y;
            break;
    }
    
    //[self startConduct];
}

- (void)init2 {
    _isDown = true;
    
    _beatX = Beat4X;
    _beatY = Beat4Y;
}

-(int)getBeatCount
{
    //Musically, a new beat starts when the hands are at the lowest point
    if (_isDown)
    {
        return (_currentBeat + _numBeats - 1) % _numBeats + 1;
    }
    else
    {
        return _currentBeat + 1;
    }
}


-(void)initWithFrame:(CGRect)frame
{
    SKSpriteNode *body = [SKSpriteNode spriteNodeWithImageNamed:@"musician_body"];
    body.xScale = body.yScale = 0.5;
    body.position = CGPointMake(CGRectGetMidX(frame),
                                CGRectGetMidY(frame));
    //[self addChild:body];
    
    _leftHand = [SKSpriteNode spriteNodeWithImageNamed:@"musician_hand"];
    _leftHand.xScale = _leftHand.yScale = 0.5;
    _leftHand.position = CGPointMake(frame.size.width / 4,
                                     CGRectGetMidY(frame));
    //[self addChild:_leftHand];
    
    
    _rightHand = [SKSpriteNode spriteNodeWithImageNamed:@"musician_hand"];
    _rightHand.xScale = -0.5;
    _rightHand.yScale = 0.5;
    _rightHand.position = CGPointMake(frame.size.width / 4 * 3,
                                      CGRectGetMidY(frame));
    //[self addChild:_rightHand];
    
}





@end
