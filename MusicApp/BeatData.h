//
//  BeatData.h
//  MusicApp
//
//  Created by hyh on 11/15/15.
//  Copyright (c) 2015 hyh. All rights reserved.
//

#ifndef MusicApp_BeatData_h
#define MusicApp_BeatData_h

float Beat2X[] = {0, 70};
float Beat2Y[] = {200, 100};

float Beat3X[] = {0, -70, 70};
float Beat3Y[] = {200, 100, 100};

float Beat4X[] = {0, 70, -70, 70};
float Beat4Y[] = {200, 100, 120, 100};

float Beat6X[] = {0, 70, 0, -70, 0, 70};
float Beat6Y[] = {200, 100, 100, 150, 100, 100};

#endif
