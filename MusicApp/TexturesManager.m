//
//  TexturesManager.m
//  MusicApp
//
//  Created by Yaroslav on 7/6/16.
//  Copyright © 2016 hyh. All rights reserved.
//

#import "TexturesManager.h"

@interface TexturesManager()

@property (nonatomic ,strong) NSMutableDictionary* cachedTextures;

@property (nonatomic, strong, readwrite) NSArray<SKTexture *> *leftConfettiTextures;
@property (nonatomic, strong, readwrite) NSArray<SKTexture *> *rightConfettiTextures;

@property (nonatomic, strong, readwrite) NSArray<SKTexture *> *leftSmokeTextures;
@property (nonatomic, strong, readwrite) NSArray<SKTexture *> *rightSmokeTextures;

@property (nonatomic, strong, readwrite) NSArray<SKTexture *> *listenWaitPlayTextures;
@property (nonatomic, strong, readwrite) NSArray<SKTexture *> *waitLightTextures;

@property (nonatomic, strong, readwrite) NSArray<SKTexture *> *leftArmTextures;
@property (nonatomic, strong, readwrite) NSArray<SKTexture *> *rightArmTextures;
@property (nonatomic, strong, readwrite) NSArray<SKTexture *> *leftDishTextures;
@property (nonatomic, strong, readwrite) NSArray<SKTexture *> *rightDishTextures;

@property (nonatomic, strong, readwrite) NSArray<SKTexture *> *drumsetHitTextures;

@end

@implementation TexturesManager

#pragma mark - Initializers

+ (instancetype)sharedInstance {
  static TexturesManager* manager = nil;
  static dispatch_once_t onceToken;
  dispatch_once(&onceToken, ^{
    manager = [[TexturesManager alloc] init];
  });
  return manager;
}

- (instancetype)init {
  self = [super init];
  if (self) {
    _cachedTextures = [NSMutableDictionary new];
    
    [self _preloadAllAvailableFaceEmotions];
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(_cleanCachedTextures)
                                                 name:UIApplicationDidReceiveMemoryWarningNotification
                                               object:nil];
  }
  return self;
}

- (void)_preloadAllAvailableFaceEmotions {
  NSArray* expressionsList = @[@(DrummerExpressionDevil),
                               @(DrummerExpressionHappy),
                               @(DrummerExpressionJoy),
                               @(DrummerExpressionNeutral),
                               @(DrummerExpressionSad0),
                               @(DrummerExpressionSad1),
                               @(DrummerExpressionSerious)];
  
  NSArray* headbanhIntencitiesList = @[@(HeadbangIntensityStrong),
                                       @(HeadbangIntensityMedium),
                                       @(HeadbangIntensityLight)];
  
  for (NSNumber* expressionNumber in expressionsList)
    for (NSNumber* headbangIntensityNumber in headbanhIntencitiesList) {
      DrummerExpression expression = expressionNumber.integerValue;
      HeadbangIntensity headbangIntencity = headbangIntensityNumber.integerValue;
      NSArray* textures = [self faceTexturesForExpression:expression
                                        headbangIntensity:headbangIntensityNumber.integerValue];
      [self _cacheTextures:textures
             forExpression:expression
         headbangIntensity:headbangIntencity];
  }
  
  [self listenWaitPlayTextures];
}

#pragma mark - Custom Setters & Getters

- (NSArray<SKTexture *> *)listenWaitPlayTextures {
  if (_listenWaitPlayTextures == nil) {
    _listenWaitPlayTextures = [self _texturesArrayWithTextureName:@"ListenWaitPlay_"
                                                   numberOfFrames:4];
  }
  return _listenWaitPlayTextures;
}

- (NSArray<SKTexture *> *)waitLightTextures {
  if (_waitLightTextures == nil) {
    _waitLightTextures =  [self _texturesArrayWithTextureName:@"yellowlight"
                                              numberOfFrames:2];
  }
  return _waitLightTextures;
}

- (NSArray<SKTexture *> *)leftConfettiTextures {
  if (_leftConfettiTextures == nil) {
    _leftConfettiTextures = [self _texturesArrayWithTextureName:@"fx"
                                                 numberOfFrames:3];
  }
  return _leftConfettiTextures;
}

- (NSArray<SKTexture *> *)rightConfettiTextures {
  if (_rightConfettiTextures == nil) {
    _rightConfettiTextures = [self _texturesArrayWithTextureName:@"fxa"
                                                  numberOfFrames:3];
  }
  return _rightConfettiTextures;
}

- (NSArray<SKTexture *> *)leftSmokeTextures {
  if (_leftSmokeTextures == nil) {
    _leftSmokeTextures = [self _texturesArrayWithTextureName:@"smokeb"
                                             numberOfFrames:7];
  }
  return _leftSmokeTextures;
}

- (NSArray<SKTexture *> *)rightSmokeTextures {
  if (_rightSmokeTextures == nil) {
    _rightSmokeTextures = [self _texturesArrayWithTextureName:@"smoke"
                                              numberOfFrames:7];
  }
  return _rightSmokeTextures;
}

- (NSArray<SKTexture *> *)leftArmTextures {
  if (_leftArmTextures == nil) {
    _leftArmTextures = [self _texturesArrayWithTextureName:@"LEFTARM"
                                           numberOfFrames:2
                                         backToFirstFrame:YES];
  }
  return _leftArmTextures;
}

- (NSArray<SKTexture *> *)leftDishTextures {
  if (_leftDishTextures == nil) {
    _leftDishTextures = [self _texturesArrayWithTextureName:@"hihat"
                                            numberOfFrames:3
                                          backToFirstFrame:YES];
  }
  return _leftDishTextures;
}

- (NSArray<SKTexture *> *)rightArmTextures {
  if (_rightArmTextures == nil) {
    _rightArmTextures = [self _texturesArrayWithTextureName:@"RIGHTARM"
                                            numberOfFrames:2
                                          backToFirstFrame:YES];
  }
  return _rightArmTextures;
}

- (NSArray<SKTexture *> *)rightDishTextures {
  if (_rightDishTextures == nil) {
    _rightDishTextures = [self _texturesArrayWithTextureName:@"right_dish"
                                             numberOfFrames:4
                                           backToFirstFrame:YES];
  }
  return _rightDishTextures;
}

- (NSArray<SKTexture *> *)drumsetHitTextures {
  if (_drumsetHitTextures == nil) {
    _drumsetHitTextures = [self _texturesArrayWithTextureName:@"drum hit"
                                               numberOfFrames:2
                                             backToFirstFrame:YES];
  }
  return _drumsetHitTextures;
}

#pragma mark - Class Methods

- (NSArray<SKTexture *> *)faceTexturesForExpression:(DrummerExpression)expression
                                  headbangIntensity:(HeadbangIntensity)intensity {
  NSArray<SKTexture *> *cachedTextures = [self _cachedTexturesForExpression:expression
                                                          headbangIntensity:intensity];
  if (cachedTextures) {
    //TEMP: Should be removed
    if (intensity == HeadbangIntensityNone) {
      cachedTextures = @[[cachedTextures firstObject]];
    }
    return cachedTextures;
  }
  
  NSMutableArray<SKTexture *> * texturesMutable = [NSMutableArray new];
  NSInteger numberOfTextures = (intensity == HeadbangIntensityStrong ? 4 : 2);
  for (int i = 0; i < numberOfTextures; i++) {
    NSString* textureName = [self _textureNameForExpression:expression
                                                  intensity:intensity
                                               textureIndex:(i + 1)];
    SKTexture* texture = [SKTexture textureWithImageNamed:textureName];
    NSParameterAssert(texture);
    [texturesMutable addObject:texture];
  }
  if (intensity == HeadbangIntensityStrong) {
    [texturesMutable addObject:[[texturesMutable firstObject] copy]];
  }
  NSArray<SKTexture *> *textures = [NSArray arrayWithArray:texturesMutable];
  return textures;
}

#pragma mark - Helpers

- (NSArray<SKTexture *> *)_texturesArrayWithTextureName:(NSString *)texturesName
                                        numberOfFrames:(NSInteger)numberOfFrames {
  return [self _texturesArrayWithTextureName:texturesName
                             numberOfFrames:numberOfFrames
                           backToFirstFrame:NO];
}

- (NSArray<SKTexture *> *)_texturesArrayWithTextureName:(NSString *)texturesName
                                        numberOfFrames:(NSInteger)numberOfFrames
                                      backToFirstFrame:(BOOL)backToFirstFrame {
  NSMutableArray* texturesMutable = [NSMutableArray arrayWithCapacity:numberOfFrames];
  for (int i = 0; i < numberOfFrames; i++) {
    NSString* textureName = [NSString stringWithFormat:@"%@%d", texturesName, i + 1];
    SKTexture* texture = [SKTexture textureWithImageNamed:textureName];
    NSParameterAssert(texture);
    [texturesMutable addObject:texture];
  }
  if (backToFirstFrame) {
    [texturesMutable addObject:[SKTexture textureWithImageNamed:[NSString stringWithFormat:@"%@1", texturesName]]];
  }
  return [NSArray arrayWithArray:texturesMutable];
}

- (NSArray<SKTexture *> *)_cachedTexturesForExpression:(DrummerExpression)expression
                                     headbangIntensity:(HeadbangIntensity)intensity {
  NSString* hashString = [self _hashStringForExpression:expression
                                      headbangIntensity:intensity];
  return [_cachedTextures objectForKey:hashString];
}

- (void)_cacheTextures:(NSArray<SKTexture *> *)textures
         forExpression:(DrummerExpression)expression
     headbangIntensity:(HeadbangIntensity)intensity {
  NSString* hashString = [self _hashStringForExpression:expression
                                      headbangIntensity:intensity];
  [_cachedTextures setObject:textures
                      forKey:hashString];
}

- (void)_cleanCachedTextures {
  [_cachedTextures removeAllObjects];
  _rightConfettiTextures = nil;
  _leftConfettiTextures = nil;
  _listenWaitPlayTextures = nil;
  _waitLightTextures = nil;
}

- (NSString *)_hashStringForExpression:(DrummerExpression)expression
                     headbangIntensity:(HeadbangIntensity)intensity {
  NSString* hashString = [NSString stringWithFormat:@"%@_%@", [self _prefixForExpression:expression], [self _postfixForHeadbangIntensity:intensity]];
  return hashString;
}

- (NSString *)_textureNameForExpression:(DrummerExpression)expression
                              intensity:(HeadbangIntensity)intensity
                           textureIndex:(NSInteger)textureIndex {
  NSString* prefix = [self _prefixForExpression:expression];
  NSString* postfix = [self _postfixForHeadbangIntensity:intensity];
  NSString* textureName = [NSString stringWithFormat:@"%@ %@%d", prefix, postfix, (int)textureIndex];
  return textureName;
}

- (NSString *)_prefixForExpression:(DrummerExpression)expression {
  NSString* prefix = @"";
  switch (expression) {
    case DrummerExpressionDevil: prefix = @"devilish"; break;
    case DrummerExpressionJoy: prefix = @"joy"; break;
    case DrummerExpressionSad0: prefix = @"sad"; break;
    case DrummerExpressionSad1: prefix = @"sad2"; break;
    case DrummerExpressionSerious: prefix = @"serious"; break;
    case DrummerExpressionHappy: prefix = @"happy"; break;
    case DrummerExpressionNeutral: prefix = @"neutral"; break;
  }
  return prefix;
}

- (NSString *)_postfixForHeadbangIntensity:(HeadbangIntensity)intensity {
  NSString* postfix = @"";
  switch (intensity) {
    case HeadbangIntensityStrong: postfix = @"headbang"; break;
    case HeadbangIntensityMedium: postfix = @"middle"; break;
    case HeadbangIntensityLight: postfix = @"light"; break;
    case HeadbangIntensityNone: postfix = @"light";
  }
  return postfix;
}

@end
