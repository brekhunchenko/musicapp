//
//  ListenWaitPlaySprite.m
//  MusicApp
//
//  Created by Yaroslav on 7/7/16.
//  Copyright © 2016 hyh. All rights reserved.
//

#import "ListenWaitPlaySprite.h"
#import "TexturesManager.h"

@interface ListenWaitPlaySprite()

@property (nonatomic, strong) SKSpriteNode* listenSpriteNode;
@property (nonatomic, strong) SKSpriteNode* waitSpriteNode;
@property (nonatomic, strong) SKSpriteNode* playSpriteNode;

@end

@implementation ListenWaitPlaySprite

#pragma mark - Initializers

- (instancetype)initWithCoder:(NSCoder *)aDecoder {
  self = [super initWithCoder:aDecoder];
  if (self) {
    [self _setupListenSpriteNode];
    [self _setupWaitSpriteNode];
    [self _setupPlaySpriteNode];
  }
  return self;
}

#pragma mark - Setup

- (void)_setupListenSpriteNode {
  _listenSpriteNode = [[SKSpriteNode alloc] initWithTexture:[SKTexture textureWithImageNamed:@"redlight"]
                                                      color:[UIColor blackColor]
                                                       size:self.size];
  _listenSpriteNode.anchorPoint = CGPointMake(0.5f, 0.0f);
  _listenSpriteNode.position = CGPointZero;
  _listenSpriteNode.hidden = YES;
  [self addChild:_listenSpriteNode];
}

- (void)_setupWaitSpriteNode {
  _waitSpriteNode = [[SKSpriteNode alloc] initWithTexture:[SKTexture textureWithImageNamed:@"yellowlight1"]
                                                    color:[UIColor blackColor]
                                                     size:self.size];
  _waitSpriteNode.anchorPoint = CGPointMake(0.5f, 0.0f);
  _waitSpriteNode.position = CGPointZero;
  _waitSpriteNode.hidden = YES;
  [self addChild:_waitSpriteNode];
}

- (void)_setupPlaySpriteNode {
  _playSpriteNode = [[SKSpriteNode alloc] initWithTexture:[SKTexture textureWithImageNamed:@"green light2"]
                                                    color:[UIColor blackColor]
                                                     size:self.size];
  _playSpriteNode.anchorPoint = CGPointMake(0.5f, 0.0f);
  _playSpriteNode.position = CGPointZero;
  _playSpriteNode.hidden = YES;
  [self addChild:_playSpriteNode];
}

#pragma mark - 

- (void)turnOffLights {
  _listenSpriteNode.hidden = YES;
  _waitSpriteNode.hidden = YES;
  [_waitSpriteNode removeAllActions];
  _playSpriteNode.hidden = YES;
}

- (void)turnOnListenLight {
  _listenSpriteNode.hidden = NO;
  _waitSpriteNode.hidden = YES;
  _playSpriteNode.hidden = YES;
}

- (void)turnOnWaitLight {
  _listenSpriteNode.hidden = YES;
  _waitSpriteNode.hidden = NO;
  _playSpriteNode.hidden = YES;
  [_waitSpriteNode runAction:[SKAction animateWithTextures:[TexturesManager sharedInstance].waitLightTextures
                                              timePerFrame:0.3f]];
}

- (void)turnOnPlayLight {
  _listenSpriteNode.hidden = YES;
  _waitSpriteNode.hidden = YES;
  _playSpriteNode.hidden = NO;
}

@end
