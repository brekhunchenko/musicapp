//
//  ConductorController.h
//  MusicApp
//
//  Created by hyh on 11/15/15.
//  Copyright (c) 2015 hyh. All rights reserved.
//

#import <Foundation/Foundation.h>

@protocol ConductorControllerDelegate;

@interface ConductorController : NSObject

@property (nonatomic, weak) id<ConductorControllerDelegate> delegate;

@property int beatNumber;
@property float beatDuration;
@property float noteDuration;

-(BOOL)isConducting;
-(void)startConduct;
-(void)stopConduct;
-(int)getCurrentBeat;
-(float)getCurrentBeatTime;
-(float)getCurrentNoteTime;
-(void)update:(float)dt;

@end

@protocol ConductorControllerDelegate <NSObject>

- (void)update:(float)dt conductor:(ConductorController *)conductor;

@end
